import React from "react";
import './header.css'

export default class Header extends React.Component {
    render() {
        return (
            <div className="header">
                <div className="logo">
                    <img
                        alt=""
                        src="https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_location_on_48px-512.png"
                    />
                    <div className="siteName">
                        <h1>Test site page</h1>
                    </div>
                </div>
                <div className="navBar">
                    <div className="navItem">
                        <button>Submit for review</button>
                    </div>
                    <div className="navItem">
                        <button>Personal Area</button>
                    </div>
                </div>
            </div>
        )
    }
}