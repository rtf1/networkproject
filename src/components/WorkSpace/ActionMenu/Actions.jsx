import React from "react";

export default class Actions extends React.Component {
    constructor(props) {
        super(props);
        this.getAction = this.getAction.bind(this);
    }
    getAction = (action) => {
        return (
            <div className="posibleActionItem" onClick={action.func} key={action}>
                <img src="https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_location_on_48px-512.png" alt="" />
            </div>
        )
    }
    render() {
        return (
            <div className="posibleActions">
                <h3>Actions</h3>
                <div className="posibleActionsMenu">
                    {this.props.actions.map(this.getAction)}
                </div>
            </div>
        )
    }
}