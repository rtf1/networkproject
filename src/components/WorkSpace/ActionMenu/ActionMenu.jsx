import React from "react";
import Actions from "./Actions";
import Resources from "./Resources";

export default class ActionsMenu extends React.Component {
    render() {
        return (
            <div className="panelOfControl">
                <Actions actions={this.props.funcList} />
                <Resources />
            </div>
        )
    }
}