import React from "react";
import ActionsMenu from "./ActionMenu/ActionMenu";
import SideBar from "./SideBar/SideBar";
import WorkZone from "./WorkZone/WorkZone";

import DeviceInfo from "./SideBar/InfoPanel/InfoPanel-presets/DeviceInfo"
import ConsoleLog from "./SideBar/InfoPanel/InfoPanel-presets/ConsoleLog"
import ControlPanel from "./SideBar/ControlPanel"

import './workSpace.css'

export default class WorkSpace extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            devices: new Map(),
            targetDevice: null,
        }

        this.switchTarget = this.switchTarget.bind(this);
    }
    switchTarget = (guid) => this.setState({ targetDevice: guid })

    randomInteger(min, max) {
        let rand = min + Math.random() * (max + 1 - min);
        return Math.floor(rand);
    }

    addNewDevice = () => {
        const { devices } = this.state;
        const newDevice = {
            guid: this.randomInteger(1, 100),
            data: this.randomInteger(1, 100),
        };
        devices.set(newDevice.guid, newDevice.data);
        this.setState({
            devices
        })
    }
    getWorkZoneData = () => {
        return {
            switchTargetFunc: this.switchTarget,
            devices: this.state.devices,
        }
    }
    getSideBarData = () => {
        const sideBarData = new Map();
        sideBarData
            .set("Device info", <DeviceInfo deviceGuid={this.state.targetDevice} />)
            .set("Console log", <ConsoleLog />)
            .set("Control panel", <ControlPanel selected={this.state.targetDevice} onAdd={() => this.addNewDevice()} />);

        return sideBarData;
    }
    getActionsMenuData = () => {
        return [
            {
                name: "Создать компьютер",
                func: this.addNewDevice,
            }
        ]
    }
    render() {
        return (
            <div className="workSpace">
                <WorkZone data={this.getWorkZoneData()} />
                <SideBar data={this.getSideBarData()} />
                <ActionsMenu funcList={this.getActionsMenuData()} />
            </div>
        )
    }
}