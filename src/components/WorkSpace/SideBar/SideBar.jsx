import React from "react";
import InfoPanel from "./InfoPanel/InfoPanel";
import ControlsMenu from "./ControlsMenu/ControlsMenu";

export default class SideBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeMenu: "Device info"
        }
        this.switchActiveMenu = this.switchActiveMenu.bind(this)
    }
    switchActiveMenu(menuName) {
        this.setState({
            activeMenu: menuName
        })
    }
    render() {
        return (
            <div className="sideBar">
                <InfoPanel
                    name={this.state.activeMenu}
                    data={this.props.data.get(this.state.activeMenu)}
                />
                <ControlsMenu
                    contorlsName={this.props.data.keys()}
                    switchFunc={this.switchActiveMenu}
                />
            </div>
        )
    }
}