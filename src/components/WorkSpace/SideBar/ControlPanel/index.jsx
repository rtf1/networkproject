import React from "react";
export default class ControlPanel extends React.Component {
   constructor(props) {
      super(props);
      this.state = {}
   }
   render() {
      const { selected } = this.props;
      return (<>
         <div>
            <button onClick={() => this.props.onAdd()}>Добавить</button>
            {selected ? <>
               <div>Type: {selected.type}</div>
               <div>ID: {selected.id}</div>
               <br />
               <button onClick={() => this.props.onDelete()}> Delete</button></> : null}
         </div>
      </>);
   }
}