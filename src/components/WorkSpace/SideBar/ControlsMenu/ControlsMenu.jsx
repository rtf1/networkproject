import React from "react";

export default class DeviceInfo extends React.Component {
    constructor(props) {
        super(props)

        this.getControl = this.getControl.bind(this);
    }

    getControl = (controlName, switchFunc) => {
        return (
            <div className="sideBarControlsItem" onClick={() => switchFunc(controlName)}>
                <img src="https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_location_on_48px-512.png" alt="" />
            </div>
        )
    }

    render() {
        return (
            <div className="sideBarControlsMenu">
                {Array.from(this.props.contorlsName).map((controlName) => this.getControl(controlName, this.props.switchFunc))}
            </div>
        )
    }
}