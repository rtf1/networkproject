import React from "react";

export default class InfoPanel extends React.Component {
    render() {
        return (
            <div className="infoPanel">
                <div className="infoPanel-name">
                    <h3>{this.props.name}</h3>
                </div>
                <div className="infoPanel-data">
                    {this.props.data}
                </div>
            </div>
        )
    }
}