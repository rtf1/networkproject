import React from "react";
import { FlowChart, actions } from "@mrblenny/react-flow-chart";
import { cloneDeep, mapValues } from "lodash";
// import { chartSimple } from "./config";
export default class TestZone extends React.Component {
  state = cloneDeep({
    offset: {
      x: 0,
      y: 0,
    },
    nodes: {
      node1: {
        id: "node1",
        type: "output-only",
        position: {
          x: 300,
          y: 100,
        },
        ports: {
          port1: {
            id: "port1",
            type: "output",
          },
        },
      },
      node2: {
        id: "node2",
        type: "input-output",
        position: {
          x: 300,
          y: 300,
        },
        ports: {
          port1: {
            id: "port1",
            type: "input",
          },
        },
      },
    },
    links: {
    },
    selected: {},
    hovered: {},
  });
  render() {
    console.log(this.props);
    // const data = {
    //   ['node' + x.key]: {
    //     id: x.key
    //   }
    // };
    const chart = this.state;
    const stateActions = mapValues(actions, (func) =>
      (...args) => this.setState(func(...args)))
    return (
      <div className="chart">
        <div id="flowchart">
          <FlowChart callbacks={{
            ...stateActions,
            onDeleteKey: this.props.onDeleteKey
          }} chart={chart} />
        </div>
      </div>
    );
  }
}
function Message(props) {
  return (
    <div
      style={{
        margin: 10,
        padding: 10,
        lineHeight: "1.4em",
      }}
    >
      {props.children}
    </div>
  );
}
function Button(props) {
  return (
    <div
      onClick={props.onClick}
      style={{
        padding: "10px 15px",
        background: "cornflowerblue",
        color: "white",
        borderRadius: "3px",
        textAlign: "center",
        transition: "0.3s ease all",
        cursor: "pointer",
      }}
    >
      {props.children}
    </div>
  );
}
