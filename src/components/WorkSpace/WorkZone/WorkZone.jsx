import React from "react";
import Draggable from "./Draggable";
import TestZone from "./TestZone";

export default class WorkZone extends React.Component {

    constructor(props) {
        super(props);

        this.getDevice = this.getDevice.bind(this);
    }

    getDevice = (deviceGuid, onClickAction) => {
        return (
            <Draggable>
                <div class="computer" onClick={() => onClickAction(deviceGuid)}>
                    <img src="https://nanotech.uz/wp-content/uploads/2018/06/Circle-icons-computer.svg_.png" alt="" />
                </div>
            </Draggable>
        )
    }

    render() {
        return (
            <div className="workTable">
                <TestZone devices={this.props.data.devices} />
                {/* {Array.from(this.props.data.devices.keys()).map((device) =>
                    this.getDevice(device, this.props.data.switchTargetFunc))} */}
            </div>
        )
    }
}