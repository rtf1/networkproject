import React from "react";
import ReactDOM from "react-dom";
class Draggable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            relX: 0,
            relY: 0,
            x: props.x,
            y: props.y,
            fixed: false,
        };
        this.gridX = props.gridX || 2;
        this.gridY = props.gridY || 2;
        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
    }
    // shouldComponentUpdate(nextProps, nextState) {
    //   return this.state.x !== nextState.x || this.state.y !== nextState.y;
    // }
    onStart(e) {
        const ref = ReactDOM.findDOMNode(this.handle);
        const body = document.body;
        const box = ref.getBoundingClientRect();
        this.setState({
            relX: e.pageX - (box.left + body.scrollLeft - body.clientLeft),
            relY: e.pageY - (box.top + body.scrollTop - body.clientTop),
        });
    }

    onMove(e) {
        const x = Math.trunc((e.pageX - this.state.relX) / this.gridX) * this.gridX;
        const y = Math.trunc((e.pageY - this.state.relY) / this.gridY) * this.gridY;
        const worktable = document
            .getElementsByClassName("workTable")[0]
            .getBoundingClientRect();
        console.log(worktable.offsetWidth, worktable.offsetHeight, x, y);
        if (
            x > worktable.left && //ограничение перемещения по worktab
            x < worktable.right &&
            y > worktable.top &&
            y < worktable.height
            //y < window.screen.height
        ) {
            if (x !== this.state.x || y !== this.state.y) {
                this.setState({
                    x,
                    y,
                });
                this.props.onMove && this.props.onMove(this.state.x, this.state.y);
            }
        }
    }

    onMouseDown(e) {
        if (e.button !== 0) return;
        if (this.state.fixed) this.setState({ fixed: false });
        this.onStart(e);
        document.addEventListener("mousemove", this.onMouseMove);
        document.addEventListener("mouseup", this.onMouseUp);
        e.preventDefault();
    }

    onMouseUp(e) {
        document.removeEventListener("mousemove", this.onMouseMove);
        document.removeEventListener("mouseup", this.onMouseUp);
        this.props.onStop && this.props.onStop(this.state.x, this.state.y);
        e.preventDefault();
    }

    onMouseMove(e) {
        this.onMove(e);
        e.preventDefault();
    }

    render() {
        return (
            <div
                onMouseDown={this.onMouseDown}
                style={{
                    position: "absolute",
                    left: this.state.x,
                    top: this.state.y,
                    touchAction: "none",
                }}
                ref={(div) => {
                    this.handle = div;
                }}
            >
                {this.props.children}
            </div>
        );
    }
}

export default Draggable;
