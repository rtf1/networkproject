import React, { Component } from 'react';
import { Container } from 'reactstrap';
import Header from './components/Header/Header';
 
export default class Layout extends Component {
  static displayName = Layout.name;

  render () {
    return (
      <div>
        <Header />
        <Container>
          {this.props.children}
        </Container>
      </div>
    );
  }
}
