import React, { Component } from 'react';
import { Route } from 'react-router';
import Layout from './Layout';
import Home from './components/Home/Home'
import Workspace from './components/WorkSpace/WorkSpace'

import './App.css'

export default class App extends Component {
  render() {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route exact path='/workspace' component ={Workspace} />
      </Layout>
    );
  }
}